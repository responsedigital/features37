<!-- Start Features37 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Columns of images, text and cta -->
@endif
<div class="features-37" is="fir-features-37" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="features-37__wrap">
      Pinecone: Features37 / Features37
  </div>
</div>
<!-- End Features37 -->