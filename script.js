class Features37 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures37()
    }

    initFeatures37 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features37")
    }

}

window.customElements.define('fir-features-37', Features37, { extends: 'div' })
